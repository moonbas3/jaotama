<?php do_action( 'jaotama_before' ); ?>

    <div class="<?php echo $main_class; ?><?php echo ( $extra_class ) ? ' ' . $extra_class : ''; ?>">

        <?php if ( $show_title ) :

            do_action( 'jaotama_before_title' ); ?>

            <div class="<?php echo $title_class; ?>">

                <?php echo apply_filters( 'jaotama_title', __( 'Share', 'jaotama' ) ); ?>

            </div>

            <?php do_action( 'jaotama_after_title' );

        endif;

        do_action( 'jaotama_before_list' ); ?>

        <ul class="<?php echo $list_class; ?>">
            <?php echo $item; ?>
        </ul>

        <?php do_action( 'jaotama_after_list' ); ?>

    </div>

<?php do_action( 'jaotama_after' );
