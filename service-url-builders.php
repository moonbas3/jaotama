<?php

namespace Jaotama;

function get_facebook_url() {
    $query_args = apply_filters( 'jaotama_facebook_args', [
        'u' => determine_page_url(),
    ] );

    return add_query_arg( $query_args, apply_filters( 'jaotama_facebook_url', 'https://www.facebook.com/sharer.php' ) );
}

function get_twitter_url() {
    $query_args = apply_filters( 'jaotama_twitter_args', [
        'url'  => determine_page_url(),
        'text' => determine_page_title(),
    ] );

    return add_query_arg( $query_args, apply_filters( 'jaotama_twitter_share_url', 'https://twitter.com/intent/tweet' ) );
}

function get_pinterest_url() {
    $query_args = apply_filters( 'jaotama_pinterest_args', [
        'url'         => determine_page_url(),
        'description' => determine_page_title(),
    ] );

    return add_query_arg( $query_args, apply_filters( 'jaotama_pinterest_share_url', 'https://pinterest.com/pin/create/bookmarklet/' ) );
}

function get_linkedin_url() {
    $query_args = apply_filters( 'jaotama_linkedin_args', [
        'url'   => determine_page_url(),
        'title' => determine_page_title(),
    ] );

    return add_query_arg( $query_args, apply_filters( 'jaotama_linkedin_share_url', 'https://www.linkedin.com/shareArticle' ) );
}

function get_google_plus_url() {
    $query_args = apply_filters( 'jaotama_google_plus_args', [
        'url' => determine_page_url(),
    ] );

    return add_query_arg( $query_args, apply_filters( 'jaotama_google_plus_share_url', 'https://plus.google.com/share' ) );
}

function get_email_url() {
    $query_args = apply_filters( 'jaotama_email_args', [
        'subject' => determine_page_title(),
        'body'    => determine_page_url(),
    ] );

    return add_query_arg( $query_args, apply_filters( 'jaotama_email_share_url', 'mailto:' ) );
}
