(function () {

    const links = document.querySelectorAll(jaotama_window_opener_class)

    links.forEach(link => link.addEventListener('click', openShareDialog))

    function openShareDialog (e) {

        e.preventDefault()

        let url    = this.getAttribute('href'),
            width  = 600,
            height = 300,
            top    = window.top.outerHeight / 2 + window.top.screenY - (height / 2),
            left   = window.top.outerWidth / 2 + window.top.screenX - (width / 2)

        window.open(url, '', `width=${width}, height=${height}, top=${top}, left=${left}`)

    }

})()