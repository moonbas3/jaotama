# Jaotama

In [Estonian](https://en.wikipedia.org/wiki/Estonia) the word "jaotama" can mean: spread, distribute, share.

## Background

Nudging people to share links on social media became the norm. There are very few website where you can't see social sharing links.

This plugin tries to give a helping hand to developers by offering most of the functionalities without assuming to much about the website.  

If you are looking for a plugin that works out of box after installing it then this is not your plugin.

## Usage

In order to display the list of social sharing links you have to call the `\Jaotama\display();` function.

## Customization

#### CSS classes

By default the plugin uses the "jaotama" base class in the markup:

```html
<div class="jaotama">
    <div class="jaotama__title">Share</div>
    <ul class="jaotama__list">
        <li class="jaotama__el jaotama__el--…">
            <a href="…" class="jaotama__window-opener" target="_self">
                <span class="jaotama__label">Share it with …</span>
                <span class="jaotama__ico">…</span>
            </a>
        </li>
    </ul>
</div>
```

You can change this by using the `jaotama_class` filter or by passing the desired class as the first argument.

```php
\Jaotama\display( 'share' );

add_filter( 'jaotama_class , function() {
    return 'share';
);
```

would result in this:

```html
<div class="share">
    <div class="share__title">Share</div>
    <ul class="share__list">
        <li>…</li>
    </ul>
</div>
```

**Other filters** that are related to the CSS classes:

- `jaotama_title_class`
- `jaotama_list_class`
- `jaotama_el_class`
- `jaotama_icon_class`

#### List caption

Before the social share links a caption is added to clarify the purpose of the list. (See the default markup above.)

You can remove the title using the `jaotama_show_title` filter or by passing `false` as the second argument to the `display` function.

```php
\Jaotama\display( null, false );

add_filter( 'jaotama_show_title , '__return_false' );
```

The `jaotama_title` filter is provided to change the text of the caption.

#### Social links

Probably one of the **most important filters** is the `jaotama_social_services`.
With this filter you can target the services' share links.

The following services are included:

- Facebook*
- Twitter*
- Google+
- LinkedIn
- Pinterest
- Email*

The services marked with `*` are the default services.

```php
add_filter( 'jaotama_social_services , function() {
    return [
        'facebook',
        'google-plus',
        'linkedin',
        'email,
    ];
);
```

**You can provide your own service** by extending the plugin.

You will notice that each link has this text by default: "Share it with SERVICE".
You can overwrite the text using the `jaotama_share_text` filter.

##### Extending the plugin

General steps to take:

1. Create a separate file which you `include` in your theme or your plugin.
2. Add the `Jaotama` namespace to the file
3. Create a function following this patter `get_SERVICE_url`
4. Return an URL to that service based on their documentation

*This is how it would look for Evernote:*

```php
namespace Jaotama;

function get_evernote_url() {
    $query_args = apply_filters( 'jaotama_evernote_args', [
        'url'   => determine_page_url(),
        'title' => determine_page_title(),
    ] );

    return add_query_arg( $query_args, apply_filters( 'jaotama_evernote_share_url', 'http://www.evernote.com/clip.action' ) );
}
```

The `determine_page_url` and `determine_page_title` functions are provided by the plugin.
These functions help to *guess* the current page URL and title but you can use your own logic to determine it.

Filters related to these functions:

- `jaotama_page_url_loop`
- `jaotama_page_url`
- `jaotama_page_title`
- `jaotama_SERVCE_args`
- `jaotama_SERVICE_url`

#### Displaying an icon

If you have the following files in your theme directory

- `your-theme/assets/svg/facebook.svg`
- `your-theme/assets/svg/twitter.svg`
- `your-theme/assets/svg/email.svg`

then the content of the SVGs will be displayed (as inline).

The plugin tries to locate the icons in the `your-theme/assets/svg/` directory;
it looks for an SVG icon matching the service names defined in the `jaotama_social_services` filter.

Use the `jaotama_show_icon` filter to disable the icon loading completely or pass `false` as the third argument.

```php
\Jaotama\display( null, null, false );

add_filter( 'jaotama_show_icon , '__return_false' );
```

Other filters related to the icons:

- `jaotama_icon_prefix`: if you want to use an unique prefix for the icons
- `jaotama_icon_location`: for changing the icons directory
- `jaotama_icon_file_type`: for changing the file icons file extension

#### New window

Clicking on links will open a new window centered in your screen instead of a new tab.
This is done using JavaScript.

You can use the `jaotama_window_opener` filter to disable this functionality.
If you are disabling the JavaScript functionality you can change the `target` of the links using the `jaotama_link_target` filter.

Other related filters:

- `jaotama_window_opener_class`
- `jaotama_window_opener_exclude`

#### Actions for the HTML markup

In case you ever need to add markup before or after the list or the title there are actions that help you: 

- `jaotama_before`
- `jaotama_before_title`
- *The **Title** is displayed here*
- `jaotama_after_title`
- `jaotama_before_list`
- *The **list** of links is displayed here*
- `jaotama_after_list`
- `jaotama_after`
